program compbrssnNS_model
IMPLICIT NONE 
!model output
! bphi_arr, aphi_arr : B, A
!bsurobs : br at the surface
!snsa : snsa(1,:) SSN North ,  snsa(2,:) SSN South
!tyrm : model time axis in year
! scalssn : calibration factor for model SSN
!nBn, nBs : North model SSN obtained by integrate B^2 over theta 
! from 1 to nBn, the South counter part from nBs to nth. 
! tacthick integration thickness of model SSN
!nr, nth, ntmax : model grid size in r, theta and time
real*8, dimension(:,:,:), allocatable :: bphi_arr, aphi_arr 
real*8, dimension(:,:), allocatable :: bsurobs
real*8, dimension(:,:), allocatable :: snsa
real*8, dimension(:), allocatable :: tyrm
real*8 :: Re_obs, Cs_obs, Comg_obs, Calph_obs
real*8, dimension(:), allocatable :: r, theta2, latitude 
real(8) :: hth, hr, invhr, invhth, rbcz
integer :: nr, nth, ntmax
integer ::ibcz, ibcz2, nBn, nBs
integer :: i, j, k, it 
real(8) :: scalssn, tacthick

character(100) :: mod_param
!observations file
!ntho, nto : theta and time grid sizes of the observation file
!tyro : time axis of obs
! pdego: polar angle axis of obs, in degrees.
!ssnNS, ssnNer : observed and processed SSN
!Br : observed and processed Br at the surface 
!real_file: real_data file

!integer :: nto, ntho
!real(8), dimension(:), allocatable ::  tyro, pdego
!real(8), dimension(:,:), allocatable :: ssnNS, ssnNSer, Br
!character(300) :: real_file 


real(8), parameter :: rtop=1.d0, rbot=0.6d0, pi=3.1415926d0, rbtor=1.d-8

integer :: nth_ssn
real(8), parameter :: th_lim=pi/8.d0
!input result file
character(300) :: result_file 
!output ascii file for plotting
character(300) :: obs_Br_ascii_file, obs_SSN_ascii_file 

integer :: nbex   !power of bphi in SSN

character(300) :: targ2, targ3, targ4

call getarg(1, mod_param)
call getarg(2, targ2)
read( targ2, '(a)') result_file
call getarg(3, targ3)
read( targ3, '(a)') obs_Br_ascii_file
call getarg(4, targ4)
read( targ4, '(a)') obs_SSN_ascii_file
! read parameters and input/output file paths.
open(unit = 10, status ='old', form='formatted',file=mod_param)
read(10,*) nr
read(10,*) nth
read(10,*) ntmax
read(10,*) rbcz 
read(10,*) tacthick 
read(10,*) scalssn
read(10,*) nbex 

close(10)
nBn=nth/2
if (mod(nth,2).eq.0) then
  nBs=nBn+1
else
  nBs=nBn+2
endif



Allocate(r(nr))
Allocate(theta2(nth))
Allocate(latitude(nth))

r=0.d0
theta2=0.d0
ibcz=0
hr=(rtop-rbot)/real((nr-1), kind=8)
invhr=1.d0/hr
DO i=1,nr
 r(i)=rbot + (i-1) * hr
 if (dabs(r(i)-rbcz).lt.rbtor) ibcz=i
ENDDO
if (ibcz .eq. 0) then
 do i=1,nr
  if ((r(i) .gt. rbcz-hr*0.5d0-rbtor) .and. (r(i) .lt. rbcz+hr*0.5d0+rbtor)) then
    ibcz=i !; print*,'ibcz: ',ibcz
  endif
 enddo
endif

i=1
do while (r(i) .lt. (rbcz+tacthick))
   i=i+1
enddo
  ibcz2=i
!write(*,*) 'ibcz2=', ibcz2
hth=pi/(nth-1)
invhth=1.d0/hth
 do j=1,nth
   theta2(j)=(j-1)*hth
   latitude(j)=90-(j-1.d0)/(nth-1.d0)*180.d0
 enddo
j=1
do while (theta2(j).LE.(0.5d0*pi-th_lim))
   j=j+1
enddo
nth_ssn=j-1


!  read model trajectory
   Allocate(bphi_arr(nr,nth,ntmax))
   bphi_arr(:,:,:)=0.0d0
   Allocate(aphi_arr(nr,nth,ntmax))
   aphi_arr(:,:,:)=0.0d0
   Allocate(bsurobs(nth,ntmax))
   bsurobs=0.d0
   Allocate(snsa(2,ntmax))
   snsa(:,:)=0.d0
   Allocate(tyrm(ntmax))
   write(*,*) 'start read est_traj'
   open(unit=1, status='old' ,form='unformatted', file=trim(result_file))
   read(unit=1) Re_obs, Cs_obs, Comg_obs, Calph_obs,tyrm,bphi_arr,aphi_arr
   close(unit=1)
!   open(unit=100, file='maxb.txt', status='unknown', form='formatted')
!   do i=1,ntmax
!     write(100,*) i, bphi_arr(ibcz2,nth/4,i)  
!   enddo
!   close(100)
!(Br)
     bsurobs(1,:)=   aphi_arr(nr,1,:)&
                    +invhth*0.5d0*(-aphi_arr(nr,3,:)+4.d0*aphi_arr(nr,2,:)&
                              -3.d0*aphi_arr(nr,1,:))*2.d0
     bsurobs(nth,:)=-aphi_arr(nr,nth,:)&
                    +invhth*0.5d0*(aphi_arr(nr,nth-2,:)-4.d0*aphi_arr(nr,nth-1,:)&
                              +3.d0*aphi_arr(nr,nth,:))*2.d0
     do i = 2, nth-1
        bsurobs(i,:)=dcos(theta2(i))/dsin(theta2(i))*aphi_arr(nr,i,:)   & 
                  +invhth*0.5d0*(aphi_arr(nr,i+1,:)-aphi_arr(nr,i-1,:))
     enddo


!  SSN ~  int B^2 
     !write(*,*) 'start at north index=', nth_ssn
     nth_ssn=1
     if (nbex.EQ.2) then
       do j=nth_ssn, nBn !nBn
        do i=ibcz,ibcz2
          snsa(1,:)=snsa(1,:) + bphi_arr(i,j,:)*bphi_arr(i,j,:)*r(i)*r(i)*dsin(theta2(j)) 
        enddo
       enddo
       do j=nBs,nth-nth_ssn+1
         do i=ibcz,ibcz2
          snsa(2,:)=snsa(2,:) + bphi_arr(i,j,:)*bphi_arr(i,j,:)*r(i)*r(i)*dsin(theta2(j)) 
         enddo
       enddo
     else
       do j=nth_ssn, nBn !nBn
        do i=ibcz,ibcz2
          snsa(1,:)=snsa(1,:) + dabs(bphi_arr(i,j,:))**real(nbex,kind=8)*r(i)*r(i)*dsin(theta2(j)) 
        enddo
       enddo
       do j=nBs,nth-nth_ssn+1
         do i=ibcz,ibcz2
          snsa(2,:)=snsa(2,:) + dabs(bphi_arr(i,j,:))**real(nbex,kind=8)*r(i)*r(i)*dsin(theta2(j)) 
         enddo
       enddo
     endif
     !write(*,*) 'end at south index=', j, nth-nth_ssn+1
     snsa(1,:)=snsa(1,:)*hr*hth*scalssn  
     snsa(2,:)=snsa(2,:)*hr*hth*scalssn    
     !write(6,*) 'done with init of obs'

!output ascii files
open(unit=97, status='unknown', form='formatted', file=trim(obs_Br_ascii_file))
!********** sample bphi at tachocline
! bsurobs(:,:) =bphi_arr(ibcz,:,:)
!**********
!write(97, '(a)')  'time (year),latitde (deg),Br (G)'
do i=1,ntmax
 do j=1,nth
!   write(97, 105)  tyrm(i),',' ,latitude(j),',' , bsurobs(j,i)
   write(97, 103)  tyrm(i), latitude(j), bsurobs(j,i)
 enddo
enddo
close(97)
open(unit=92, status='unknown', form='formatted', file=trim(obs_SSN_ascii_file))
!write(92, '(a)')  'time (year),SSN north,SSN south,SSN total'
!**********
do i=1,ntmax
!   write(92, 106)  tyrm(i), ',' ,snsa(1,i),',' , snsa(2,i),',' , snsa(1,i)+snsa(2,i)
   write(92, 104)  tyrm(i), snsa(1,i), snsa(2,i), snsa(1,i)+snsa(2,i)
enddo
close(92)
103 FORMAT(3E20.10)
104 FORMAT(4E20.10)
105 FORMAT(E20.10, A1, E20.10, A1,E20.10)
106 FORMAT(E20.10,A1,E20.10, A1, E20.10, A1,E20.10)


  deallocate(r, theta2, aphi_arr, bphi_arr, bsurobs)



!deallocate(Br, ssnNS, ssnNSer, tyro, pdego, tyrm)
deallocate(latitude,snsa)


end !subroutine !module data_obs
