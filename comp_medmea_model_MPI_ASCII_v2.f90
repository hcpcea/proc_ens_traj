program comp_medmea_model_MPI_ASCII_v2
use mpi_f08
IMPLICIT NONE 
!model output
! bphi_arr, aphi_arr : B, A
!bsurobs : br at the surface
!snsa : snsa(1,:) SSN North ,  snsa(2,:) SSN South
!tyrm : model time axis in year
! scalssn : calibration factor for model SSN
!nBn, nBs : North model SSN obtained by integrate B^2 over theta 
! from 1 to nBn, the South counter part from nBs to nth. 
! tacthick integration thickness of model SSN
!nr, nth, ntmax : model grid size in r, theta and time
real*8, dimension(:,:,:), allocatable :: bphi_arr, aphi_arr 
real*8, dimension(:,:), allocatable :: bsurobs
real*8, dimension(:,:), allocatable :: snsa, snsasd, measnsa, medssn
real*8, dimension(:), allocatable :: tyrm, ssntot, ssntotsd, medsnsa, ssntotave
real*8 :: Re_obs, Cs_obs, Comg_obs, Calph_obs
real*8, dimension(:), allocatable :: r, theta2, latitude 
real(8) :: hth, hr, invhr, invhth, rbcz
integer :: nr, nth, ntmax, nbrtot, nth2
integer ::ibcz, ibcz2, nBn, nBs
integer :: i, j, k, it 
real(8) :: scalssn, tacthick
real(8) :: solmax, solmax_sd, tmax, tmax_sd
logical :: findmax_flag
character(100) :: mod_param
character(1) :: quarter   !file naming quarter year of pred Q1/2/3/4
character(4) :: aexyear   !file naming year of pred 
integer :: iexyear        !integral part of year of pred
real(8) :: remyear        !decimal part of year of pred, determine Q1/2/3/4
integer :: exmonth        !period of prediction in months
character(3) :: aexmonth  !file naming of period of prediction 
real(8) :: endyr          !end of prediction in year
integer :: iexend         !time index end of prediction to output
!observations file
!ntho, nto : theta and time grid sizes of the observation file
!tyro : time axis of obs
! pdego: polar angle axis of obs, in degrees.
!ssnNS, ssnNer : observed and processed SSN
!Br : observed and processed Br at the surface 
!real_file: real_data file

!integer :: nto, ntho
!real(8), dimension(:), allocatable ::  tyro, pdego
!real(8), dimension(:,:), allocatable :: ssnNS, ssnNSer, Br
!character(300) :: real_file 


real(8), parameter :: rtop=1.d0, rbot=0.6d0, pi=3.1415926d0, rbtor=1.d-8
real(8), parameter :: Rsun=6.9599d10 , etatop=15.d11, cfl=1.d-6

integer :: nth_ssn
real(8), parameter :: th_lim=pi/8.d0
!model time axis
real(8), dimension(:), allocatable :: time
integer, parameter :: imf=162, itrefm=600, itrefo=1
real(8), parameter :: yrrefm=1970.d0

!input result file
character(300) :: result_file, result_br_file 
!output ascii file for plotting
character(300) :: obs_Br_ascii_file, obs_SSN_ascii_file, result_dir
character(300) :: re_ssn_file, re_br_file
character(10)  :: str_ensnum
character(100) :: outfile, outbr_avefile, outbr_sdfile

integer :: nbex   !power of bphi in SSN
real(8) :: asyr, styr, exyr, mxv, limlef, limrig,  limlefsd, limrigsd, mxv2
integer :: iasyr, itstart, istyr,iexyr, it2, limcount, limcountsum
logical :: mxfg

real(8), allocatable, dimension(:) :: solmax_arr, tmax_arr
real(8), allocatable, dimension(:,:) :: sol_all, br_arr_ave, br_arr_sd, br_arr_med, temp_med
real(8) :: temp, med_solmax, med_tmax
integer :: io_flag

!mpi variables
integer :: nprocs, rank, ierr
type(MPI_Comm) :: Comm1
type(MPI_status) :: status 
integer :: ncount

Comm1=MPI_COMM_WORLD
call MPI_Init(ierr)
call MPI_COMM_SIZE(Comm1, nprocs, ierr)
call MPI_COMM_RANK(Comm1, rank, ierr)



call getarg(1, mod_param)
! read parameters and input/output file paths.
open(unit = 10, status ='old', form='formatted',file=mod_param)
read(10,*) nr
read(10,*) nth
read(10,*) ntmax
read(10,*) rbcz 
read(10,*) tacthick 
read(10,*) scalssn
read(10,*) nbex 
read(10,*) result_dir
read(10,*) asyr 
read(10,*) styr 
read(10,*) exyr 
read(10,*) exmonth 

close(10)
iexyear=int(styr)
remyear=styr-iexyear
write(aexyear,'(I4)') iexyear
if (remyear .LT. 0.25d0) then
   quarter='1'
else if (remyear .LT. 0.5d0) then
        quarter='2'
     else if (remyear .LT. 0.75d0) then
             quarter='3'          
          else 
            quarter='4'
endif

write(aexmonth,'(I0.3)') exmonth

outfile=trim('ssn_ex'//aexyear//'Q'//quarter//'_'//aexmonth//'m.csv')
outbr_avefile=trim('br_stat_ex'//aexyear//'Q'//quarter//'.csv')
 
nBn=nth/2
if (mod(nth,2).eq.0) then
  nBs=nBn+1
else
  nBs=nBn+2
endif

Allocate(r(nr))
Allocate(theta2(nth))

r=0.d0
theta2=0.d0
ibcz=0
hr=(rtop-rbot)/real((nr-1), kind=8)
invhr=1.d0/hr
DO i=1,nr
 r(i)=rbot + (i-1) * hr
 if (dabs(r(i)-rbcz).lt.rbtor) ibcz=i
ENDDO
if (ibcz .eq. 0) then
 do i=1,nr
  if ((r(i) .gt. rbcz-hr*0.5d0-rbtor) .and. (r(i) .lt. rbcz+hr*0.5d0+rbtor)) then
    ibcz=i !; print*,'ibcz: ',ibcz
  endif
 enddo
endif

i=1
do while (r(i) .lt. (rbcz+tacthick))
   i=i+1
enddo
  ibcz2=i
!write(*,*) 'ibcz2=', ibcz2
write(str_ensnum,'(I3.3)') rank+101
result_file=trim(result_dir)//'/est_model_ssn_'//trim(str_ensnum)//'.txt'
result_br_file=trim(result_dir)//'/est_model_brad_'//trim(str_ensnum)//'.txt'
io_flag=0

ntmax=0
!write(*,*) 'result file is', result_file
open(unit=1, status='old' ,form='formatted', file=result_file)
do while (io_flag.EQ.0)
   ntmax=ntmax+1
   read(1, *, iostat=io_flag) temp
!   write(*,*) 'rank, temp =', rank, temp
enddo
ntmax=ntmax-1
close(unit=1)

io_flag=0
nbrtot=0
open(unit=1, status='old' ,form='formatted', file=result_br_file)
do while (io_flag.EQ.0)
   nbrtot=nbrtot+1
   read(1, *, iostat=io_flag) temp
!   write(*,*) 'rank, temp =', rank, temp
enddo
nbrtot=nbrtot-1
close(unit=1)
nth2=nbrtot/ntmax
Allocate(latitude(nth2))
hth=pi/(nth2-1)
invhth=1.d0/hth
 do j=1,nth2
!   theta2(j)=(j-1)*hth
   latitude(j)=90-(j-1.d0)/(nth2-1.d0)*180.d0
 enddo
j=1
!do while (theta2(j).LE.(0.5d0*pi-th_lim))
!   j=j+1
!enddo
!nth_ssn=j-1
Allocate(br_arr_sd(nth2,ntmax))
Allocate(br_arr_ave(nth2,ntmax))
Allocate(br_arr_med(nth2,ntmax))


   Allocate(snsa(2,ntmax)); Allocate(snsasd(2,ntmax))
   Allocate(measnsa(2,ntmax)); Allocate(medsnsa(ntmax)); Allocate(medssn(2,ntmax))
   snsa(:,:)=0.d0
   Allocate(ssntot(ntmax)); Allocate(ssntotsd(ntmax)); Allocate(ssntotave(ntmax)) 
   Allocate(tyrm(ntmax))
  Allocate(sol_all( ntmax, nprocs))
if (rank.eq.0) write(*,*) 'ntmax=', ntmax, 'nth2=', nth2
open(unit=2, status='old' ,form='formatted', file=result_file)
do i=1, ntmax
   read(2, *) tyrm(i), snsa(1,i), snsa(2,i), ssntot(i) 
enddo
close(unit=2)

it=1
do while (tyrm(it).LE. asyr)
  it=it+1
enddo
iasyr=it
do while (tyrm(it).LE. styr)
  it=it+1
enddo
istyr=it
do while (tyrm(it).LE. exyr)
  it=it+1
enddo
iexyr=it

open(unit=2, status='old' ,form='formatted', file=result_br_file)
do i=1, ntmax
  do j=1,nth2
   read(2, *) temp, latitude(j), br_arr_sd(j,i)
  enddo
enddo
close(unit=2)

br_arr_sd(:,1:iasyr)=0.d0
br_arr_med(:,:)=br_arr_sd(:,:)

ncount=ntmax*2
call MPI_Allreduce(snsa(1,1), measnsa(1,1), ncount, MPI_real8, MPI_sum, Comm1)
measnsa(:,:)=measnsa(:,:)/real(nprocs, kind=8)
!----jan 1 2023
snsasd(:,:)=(snsa(:,:)-measnsa(:,:))*(snsa(:,:)-measnsa(:,:))
call MPI_Allreduce(MPI_IN_PLACE,snsasd(1,1), ncount, MPI_real8, MPI_sum, Comm1)
snsasd(:,:)=dsqrt(snsasd(:,:)/real(nprocs-1, kind=8))
!----

ncount=ntmax
call MPI_Allreduce(ssntot(1),ssntotave(1), ncount, MPI_real8, MPI_sum, Comm1)
ssntotave(:)=ssntotave(:)/real(nprocs, kind=8)
ssntotsd(:)=(ssntot(:)-ssntotave(:))*(ssntot(:)-ssntotave(:))
call MPI_Allreduce(MPI_IN_PLACE,ssntotsd(1), ncount, MPI_real8, MPI_sum, Comm1)
ssntotsd(:)=dsqrt(ssntotsd(:)/real(nprocs-1, kind=8))


ncount=ntmax*nth2
call MPI_Allreduce(br_arr_sd(1,1),br_arr_ave(1,1), ncount, MPI_real8, MPI_sum, Comm1)
br_arr_ave(:,:)=br_arr_ave(:,:)/real(nprocs, kind=8)
br_arr_sd(:,:)=(br_arr_sd(:,:)-br_arr_ave(:,:))*(br_arr_sd(:,:)-br_arr_ave(:,:))
call MPI_Allreduce(MPI_IN_PLACE,br_arr_sd(1,1), ncount, MPI_real8, MPI_sum, Comm1)
br_arr_sd(:,:)=dsqrt(br_arr_sd(:,:)/real(nprocs-1, kind=8))



!if (rank.eq.0) then
   mxfg=.FALSE.
   do while (mxfg.EQ..FALSE.)
      if ((ssntotave(it).LE. ssntotave(it+1)).AND.(ssntotave(it+1).GE.ssntotave(it+2))) mxfg=.TRUE.
      it=it+1
   enddo
   mxv=ssntotave(it)
   mxfg=.FALSE.
   it2=iexyr
   do while (mxfg.EQ..FALSE.)
      if ((ssntot(it2).LE. ssntot(it2+1)).AND.(ssntot(it2+1).GE.ssntot(it2+2))) mxfg=.TRUE.
      it2=it2+1
   enddo
   mxv2=ssntot(it2)
   limcount=0
   limlef=0.; limrig=0.
   limlefsd=0.; limrigsd=0.
   if (mxv2.GT.mxv)  limcount=1
   if (limcount.EQ.1) then
      it=iexyr
      do while (ssntot(it).LE. mxv) 
       it=it+1
      enddo
      limlef=tyrm(it-1)
      do while (ssntot(it).GE. mxv) 
       it=it+1
      enddo
      limrig= tyrm(it-1)
   endif
   limlefsd=limlef
   limrigsd=limrig

   call MPI_allreduce(limcount, limcountsum, 1, MPI_int, MPI_sum, Comm1)
   call MPI_allreduce(limlef, limlefsd, 1, MPI_real8, MPI_sum, Comm1)
   call MPI_allreduce(limrig, limrigsd, 1, MPI_real8, MPI_sum, Comm1)
   limlefsd=limlefsd/limcountsum
   limrigsd=limrigsd/limcountsum
   if (limcount.EQ.0)  limlefsd=0.
   if (limcount.EQ.0)  limrigsd=0.
   limlef=(limlef-limlefsd)*(limlef-limlefsd)
   limrig=(limrig-limrigsd)*(limrig-limrigsd)
   call MPI_allreduce(MPI_IN_PLACE, limlef, 1, MPI_real8, MPI_sum, Comm1)
   call MPI_allreduce(MPI_IN_PLACE, limrig, 1, MPI_real8, MPI_sum, Comm1)
   limlef=dsqrt(limlef/(limcountsum-1.))
   limrig=dsqrt(limrig/(limcountsum-1.))
   call MPI_allreduce(limlef, limlefsd, 1, MPI_real8, MPI_sum, Comm1)
   call MPI_allreduce(limrig, limrigsd, 1, MPI_real8, MPI_sum, Comm1)
   limlefsd=limlefsd/limcountsum
   limrigsd=limrigsd/limcountsum
   if (rank.EQ.0)  then
      write(*,*) 'lef ave=',limlefsd
      write(*,*) 'lef sd=', limlef
      write(*,*) 'rig ave=',limrigsd
      write(*,*) 'rig sd=', limrig

   endif



!endif



if (rank.EQ.0) then
  Allocate(solmax_arr(nprocs), tmax_arr(nprocs))
else
  Allocate(solmax_arr(0), tmax_arr(0))
endif
call MPI_Gather(ssntot(1), ntmax, MPI_real8, sol_all(1,1), ntmax, MPI_real8, 0, MPI_COMM_WORLD, ierr)
if (rank.EQ.0) then
do it=1, ntmax
 solmax_arr(:)=sol_all(it,:)
! write(*,*) 'b4', solmax_arr(:)
 do i= nprocs-1, 2,-1
   do j=1, i 
    if (solmax_arr(j) .GE. solmax_arr(j+1)) then
       temp= solmax_arr(j)
       solmax_arr(j)=solmax_arr(j+1)
       solmax_arr(j+1)=temp
    endif    
   enddo
 enddo
! write(*,*) 'aft', solmax_arr(:)
 if (mod(nprocs,2).EQ.0) then
   med_solmax=0.5d0*(solmax_arr(nprocs/2)+solmax_arr(nprocs/2+1))
 else
   med_solmax=solmax_arr(nprocs/2+1)
 endif
 medsnsa(it)=med_solmax

enddo
endif
!--Jan 1 2023  
sol_all(:,:)=0.d0
call MPI_Gather(snsa(1,1), ntmax, MPI_real8, sol_all(1,1), ntmax, MPI_real8, 0, MPI_COMM_WORLD, ierr)
if (rank.EQ.0) then
do it=1, ntmax
 solmax_arr(:)=sol_all(it,:)
! write(*,*) 'b4', solmax_arr(:)
 do i= nprocs-1, 2,-1
   do j=1, i 
    if (solmax_arr(j) .GE. solmax_arr(j+1)) then
       temp= solmax_arr(j)
       solmax_arr(j)=solmax_arr(j+1)
       solmax_arr(j+1)=temp
    endif    
   enddo
 enddo
! write(*,*) 'aft', solmax_arr(:)
 if (mod(nprocs,2).EQ.0) then
   med_solmax=0.5d0*(solmax_arr(nprocs/2)+solmax_arr(nprocs/2+1))
 else
   med_solmax=solmax_arr(nprocs/2+1)
 endif
 medssn(1,it)=med_solmax

enddo
endif
sol_all(:,:)=0.d0
call MPI_Gather(snsa(2,1), ntmax, MPI_real8, sol_all(1,1), ntmax, MPI_real8, 0, MPI_COMM_WORLD, ierr)
if (rank.EQ.0) then
do it=1, ntmax
 solmax_arr(:)=sol_all(it,:)
! write(*,*) 'b4', solmax_arr(:)
 do i= nprocs-1, 2,-1
   do j=1, i 
    if (solmax_arr(j) .GE. solmax_arr(j+1)) then
       temp= solmax_arr(j)
       solmax_arr(j)=solmax_arr(j+1)
       solmax_arr(j+1)=temp
    endif    
   enddo
 enddo
! write(*,*) 'aft', solmax_arr(:)
 if (mod(nprocs,2).EQ.0) then
   med_solmax=0.5d0*(solmax_arr(nprocs/2)+solmax_arr(nprocs/2+1))
 else
   med_solmax=solmax_arr(nprocs/2+1)
 endif
 medssn(2,it)=med_solmax

enddo
endif
!---
deallocate(solmax_arr, tmax_arr)


endyr=styr+exmonth/12.d0            
 it=1
   do while (tyrm(it).LE. endyr)
      it=it+1
   enddo
iexend=min(it, ntmax)




if (rank.EQ.0) then
open(unit=1, file=trim(result_dir)//'/'//trim(outfile), form='formatted', status='unknown')
write(1, '(a)')  'time (year),median SSN,mean SSN,SSN SD,median SSNN,median SSNS,mean SSNN,SSNN SD,mean SSNS,SSNS SD'

!--jan 1 2023
 do it=iasyr, iexend
  write(1,109)  tyrm(it),',', medsnsa(it),',', measnsa(1,it) + measnsa(2,it),',',ssntotsd(it),',', &
         medssn(1,it), ',',medssn(2,it),',' ,snsa(1,it),',' ,snsasd(1,it),',' ,snsa(2,it),',' ,snsasd(2,it)

 enddo
!-- 
close(1)
endif

!if (rank.EQ.0) then
!open(unit=2, file=trim(result_dir)//'/br_ave.txt', form='formatted', status='unknown')
! do i=1,ntmax
!   do j=1,nth2
!      write(2, 105) tyrm(i), latitude(j), br_arr_ave(j,i)
!   enddo
! enddo
!close(2)
!open(unit=3, file=trim(result_dir)//'/br_sd.txt', form='formatted', status='unknown')
! do i=1,ntmax
!   do j=1,nth2
!      write(3, 105) tyrm(i), latitude(j), br_arr_sd(j,i)
!   enddo
! enddo
!close(3)
!endif

Allocate(temp_med(nth2,nprocs))

do it=istyr, ntmax

call MPI_Gather(br_arr_med(1,it), nth2, MPI_real8, temp_med(1,1), nth2, MPI_real8, 0, MPI_COMM_WORLD, ierr)
if (rank.EQ.0) then
 do k=1,nth2
! solmax_arr(:)=sol_all(it,:)
! write(*,*) 'b4', solmax_arr(:)
 do i= nprocs-1, 2,-1
   do j=1, i 
    if (temp_med(k,j) .GE. temp_med(k,j+1)) then
       temp= temp_med(k,j)
       temp_med(k,j)=temp_med(k,j+1)
       temp_med(k,j+1)=temp
    endif    
   enddo
 enddo
! write(*,*) 'aft', solmax_arr(:)
 if (mod(nprocs,2).EQ.0) then
   temp=0.5d0*(temp_med(k,nprocs/2)+temp_med(k,nprocs/2+1))
 else
   temp=temp_med(k, nprocs/2+1)
 endif
 br_arr_med(k,it)=temp
 enddo
endif
enddo

if (rank.EQ.0) then
open(unit=3, file=trim(result_dir)//'/br_stat.txt', form='formatted', status='unknown')
open(unit=4, file=trim(result_dir)//'/'//trim(outbr_avefile), form='formatted', status='unknown')
write(4, '(a)')  'time (year),latitude,br ave,br sd,br median'
 do i=1,ntmax
   do j=1,nth2
      write(3, 107) tyrm(i), latitude(j), br_arr_ave(j,i), br_arr_sd(j,i), br_arr_med(j,i)
      write(4, 108) tyrm(i),',',latitude(j),',',br_arr_ave(j,i),',',br_arr_sd(j,i),',',br_arr_med(j,i)
   enddo
 enddo
close(3)
close(4)
endif

deallocate(temp_med)

105 FORMAT(3E20.10)
106 FORMAT(E20.10,A1,E20.10,A1,E20.10,A1,E20.10)

107 FORMAT(5E20.10)
108 FORMAT(E20.10,A1,E20.10,A1,E20.10,A1,E20.10,A1,E20.10)
!--jan 1 2023
109 FORMAT(E20.10,A1,E20.10,A1,E20.10,A1,E20.10,A1,E20.10,A1,E20.10,A1,E20.10,A1,E20.10,A1,E20.10,A1,E20.10)
!--
  deallocate(r, theta2)!, aphi_arr, bphi_arr, bsurobs)


deallocate(measnsa, medsnsa, medssn, sol_all)
!deallocate(Br, ssnNS, ssnNSer, tyro, pdego, tyrm)
deallocate(latitude,snsa, ssntot, snsasd, ssntotsd, ssntotave)
deallocate(br_arr_sd, br_arr_ave, br_arr_med)
call MPI_FINALIZE(ierr)

end !subroutine !module data_obs
