import os, fnmatch
def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

k=find('*.csv', '/beegfs/data/chung/ens/merged_n_B9_test_ex1986/')
print(k[0])
