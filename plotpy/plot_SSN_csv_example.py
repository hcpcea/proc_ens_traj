import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import numpy as np
import matplotlib
import os, fnmatch
def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

k=find('ssn*.csv', '__OUTDIR__')

infile=k[0]
iyr=int(infile[len(infile)-15:len(infile)-11])
iq=int(infile[len(infile)-10:len(infile)-9])
fsize=20
matplotlib.rc('xtick', labelsize=fsize)
matplotlib.rc('ytick', labelsize=fsize)
minorLocator = AutoMinorLocator(4)
outplot=infile[0:len(infile)-4]+'.png'
ylb=-20; yub=250
exrealyr=iyr*1.+(iq-1)*0.25
nex=[exrealyr]
datapred1 = pd.read_csv(infile, delimiter=",")
datareal = pd.read_csv("ssn_ascii_ssn_5_cr2256_pos_pad_const.csv", delimiter=",")
xlb=np.amin(datapred1['time (year)']);xub=np.amax(datapred1['time (year)'])+2


fig = plt.figure(figsize=(10,5.5))
fig.subplots_adjust(right=0.97, top=0.97, wspace=0.29, bottom=0.15)

ax = fig.add_subplot(111)
ax.plot(datapred1['time (year)'], datapred1['mean SSN'], linewidth=1.5, linestyle='-', color='g', label='pred. SSN')
plt.fill_between(datapred1['time (year)'], datapred1['mean SSN']-datapred1['SSN SD'], datapred1['mean SSN']+datapred1['SSN SD'], alpha=0.2, edgecolor='#3F7F4C', facecolor='#7EFF99')
ax.plot(datareal['time (year)'],datareal['SSN total'], linewidth=3.0, linestyle='-', color='r', label='processed SSN')
plt.fill_between(datareal['time (year)'], datareal['SSN total']-datareal['SSN err total'], datareal['SSN total']+datareal['SSN err total'], alpha=0.2, edgecolor='#CC4F1B', facecolor='#FF9848')

ax.set_xlabel('time (year)', fontsize=fsize)
ax.set_ylabel('SSN', fontsize=fsize)
ax.set_xlim(xlb, xub)
ax.set_ylim(ylb, yub)
ax.legend(loc='best', frameon=False,  framealpha=0.5, fontsize=fsize)
plt.axvline(x=exrealyr, linestyle='-.', linewidth=2.0, color='g')
plt.axhline(y=0., linestyle='-', linewidth=2.0, color='k')
#plt.axvline(x=2013.6, linestyle='-.', linewidth=2.0, color='k')



fig.savefig(outplot)

plt.show()


