#from config_plots import *
from matplotlib.ticker import AutoMinorLocator
import matplotlib.pyplot as plt
from matplotlib import mlab, cm
import numpy as np
from numpy import sin
import matplotlib
matplotlib.rc('xtick', labelsize=15)
matplotlib.rc('ytick', labelsize=15)
fsize=15

# obs latitude grid size
ntho=127
# obs time grid size
nto=1000
# model latitude grid size
nth=129
# model time grid size
nt=5000

#xlb=1970; xub=2020
#ascii files
#IODIR='/beegfs/data/chung/da_souleu/n_B9_test_icAuto_ex2019pt5_pos_fssn00001'   #files directory
IODIR='__OUTDIR__/'   #files directory
#IODIR='/beegfs/data/chung/ens/merged_n_B9_test_ex1986/'   #files directory
#IODIR='/beegfs/data/chung/freerun_test'   #files directory
#IODIR='/beegfs/data/chung/da_souleu/newstore'   #files directory
estbr=IODIR+'/br_stat.txt'
outplot=IODIR+'/br.eps'

title2='unicellular'

#endyr=1995.


data = np.loadtxt(estbr)
modetim1 = data[:,0]
latitude = data[:,1]
u0thsurf = data[:,2]
u1thsurf = data[:,3]
u2thsurf = data[:,4]
ndg=nth
ntt=nt
Y2=np.ones((ndg))
X2=np.ones((ntt))
Z0=np.ones((ndg,ntt))
Z1=np.ones((ndg,ntt))
Z2=np.ones((ndg,ntt))
xlb=np.amin(modetim1); xub=np.amax(modetim1)

ceil=10
k=0
for i in range(0,ntt):
    for j in range(0,ndg):
        X2[i]=modetim1[k]
        Y2[j]=sin(latitude[k]*3.14159/180.0)
        Z0[j,i]=u0thsurf[k]
        Z1[j,i]=u1thsurf[k]
        Z2[j,i]=u2thsurf[k]
        k=k+1
        if Z2[j,i] < -ceil:
           Z2[j,i] = -ceil
        if Z2[j,i] > ceil:
           Z2[j,i] = ceil
sat=10.
dsat=sat*0.05
nlv=41
ncb=11
levels = np.linspace(-sat, sat, nlv)  # Boost the upper limit to avoid truncation errors.
norm = cm.colors.Normalize(vmax=sat, vmin=-sat)
#cmap = cm.PRGn
#cmap = cm.nipy_spectral
cmap = cm.seismic
#cmap = cm.jet
plt.figure(figsize=(6,6))
plt.subplots_adjust(left=0.15, right=0.95, top=0.9, wspace=0.29, hspace=0.2, bottom=0.07)
CS = plt.subplot(3, 1, 1)
CS = plt.contourf(X2, Y2, Z2, levels,
                 cmap=cm.get_cmap(cmap, len(levels) -1))

plt.xlim(left=xlb, right=xub)
#plt.xlabel('time (yr)',fontsize=fsize)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    labelbottom=False) # labels along the bottom edge are off
plt.ylabel('sin latitude', fontsize=fsize)

plt.title('Br median',fontsize=fsize)
cb = plt.colorbar(ticks=np.linspace(-sat, sat, ncb))
font_size = 15 # Adjust as appropriate.
cb.ax.tick_params(labelsize=font_size)
cb.set_label(label='$B_{r, est} \mathrm{(G)}$', size=fsize)

CS = plt.subplot(3, 1, 2)
CS = plt.contourf(X2, Y2, Z0, levels,
                 cmap=cm.get_cmap(cmap, len(levels) -1))

plt.xlim(left=xlb, right=xub)
#plt.xlabel('time (yr)',fontsize=fsize)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    labelbottom=False) # labels along the bottom edge are off
plt.ylabel('sin latitude', fontsize=fsize)

plt.title('Br average',fontsize=fsize)
cb = plt.colorbar(ticks=np.linspace(-sat, sat, ncb))
font_size = 15 # Adjust as appropriate.
cb.ax.tick_params(labelsize=font_size)
cb.set_label(label='$B_{r, est} \mathrm{(G)}$', size=fsize)

CS = plt.subplot(3, 1, 3)
CS = plt.contourf(X2, Y2, Z2, levels,
                 cmap=cm.get_cmap(cmap, len(levels) -1))

plt.xlim(left=xlb, right=xub)
plt.xlabel('time (yr)',fontsize=fsize)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    labelbottom=True) # labels along the bottom edge are off
plt.ylabel('sin latitude', fontsize=fsize)

plt.title('Br SD',fontsize=fsize)
cb = plt.colorbar(ticks=np.linspace(-sat, sat, ncb))
font_size = 15 # Adjust as appropriate.
cb.ax.tick_params(labelsize=font_size)
cb.set_label(label='$B_{r, est} \mathrm{(G)}$', size=fsize)
#plt.savefig(outplot)
plt.show()
