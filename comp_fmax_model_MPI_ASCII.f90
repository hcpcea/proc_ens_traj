program comp_fmax_model_MPI_ASCII
use mpi_f08
IMPLICIT NONE 
!model output
! bphi_arr, aphi_arr : B, A
!bsurobs : br at the surface
!snsa : snsa(1,:) SSN North ,  snsa(2,:) SSN South
!tyrm : model time axis in year
! scalssn : calibration factor for model SSN
!nBn, nBs : North model SSN obtained by integrate B^2 over theta 
! from 1 to nBn, the South counter part from nBs to nth. 
! tacthick integration thickness of model SSN
!nr, nth, ntmax : model grid size in r, theta and time
real*8, dimension(:,:,:), allocatable :: bphi_arr, aphi_arr 
real*8, dimension(:,:), allocatable :: bsurobs
real*8, dimension(:,:), allocatable :: snsa, snsasd
real*8, dimension(:), allocatable :: tyrm, ssntot, ssntotsd
real*8 :: Re_obs, Cs_obs, Comg_obs, Calph_obs
real*8, dimension(:), allocatable :: r, theta2, latitude 
real(8) :: hth, hr, invhr, invhth, rbcz
integer :: nr, nth, ntmax
integer ::ibcz, ibcz2, nBn, nBs
integer :: i, j, k, it 
real(8) :: scalssn, tacthick
real(8) :: solmax, solmax_sd, tmax, tmax_sd
logical :: findmax_flag
character(100) :: mod_param
!observations file
!ntho, nto : theta and time grid sizes of the observation file
!tyro : time axis of obs
! pdego: polar angle axis of obs, in degrees.
!ssnNS, ssnNer : observed and processed SSN
!Br : observed and processed Br at the surface 
!real_file: real_data file

!integer :: nto, ntho
!real(8), dimension(:), allocatable ::  tyro, pdego
!real(8), dimension(:,:), allocatable :: ssnNS, ssnNSer, Br
!character(300) :: real_file 


real(8), parameter :: rtop=1.d0, rbot=0.6d0, pi=3.1415926d0, rbtor=1.d-8
real(8), parameter :: Rsun=6.9599d10 , etatop=15.d11, cfl=1.d-6

integer :: nth_ssn
real(8), parameter :: th_lim=pi/8.d0
!model time axis
real(8), dimension(:), allocatable :: time
integer, parameter :: imf=162, itrefm=600, itrefo=1
real(8), parameter :: yrrefm=1970.d0

!input result file
character(300) :: result_file 
!output ascii file for plotting
character(300) :: obs_Br_ascii_file, obs_SSN_ascii_file, result_dir
character(300) :: re_ssn_file, re_br_file
character(10)  :: str_ensnum

integer :: filterflag, norder, nchop
integer :: nbex   !power of bphi in SSN
real(8) :: asyr, styr, exyr
integer :: itstart, iexyr

real(8), allocatable, dimension(:) :: solmax_arr, tmax_arr
real(8) :: temp, med_solmax, med_tmax
integer :: io_flag

!mpi variables
integer :: nprocs, rank, ierr
type(MPI_Comm) :: Comm1
type(MPI_status) :: status 
integer :: ncount

Comm1=MPI_COMM_WORLD
call MPI_Init()
call MPI_COMM_SIZE(Comm1, nprocs, ierr)
call MPI_COMM_RANK(Comm1, rank, ierr)



call getarg(1, mod_param)
! read parameters and input/output file paths.
open(unit = 10, status ='old', form='formatted',file=mod_param)
read(10,*) nr
read(10,*) nth
read(10,*) ntmax
read(10,*) rbcz 
read(10,*) tacthick 
read(10,*) scalssn
read(10,*) nbex 
read(10,*) result_dir
read(10,*) asyr 
read(10,*) styr 
read(10,*) exyr 

close(10)



write(str_ensnum,'(I3.3)') rank+101
result_file=trim(result_dir)//'/est_model_ssn_'//trim(str_ensnum)//'.txt'
io_flag=0

ntmax=0
!write(*,*) 'result file is', result_file
open(unit=1, status='old' ,form='formatted', file=result_file)
do while (io_flag.EQ.0)
   ntmax=ntmax+1
   read(1, *, iostat=io_flag) temp
!   write(*,*) 'rank, temp =', rank, temp
enddo
ntmax=ntmax-1
close(unit=1)

   Allocate(snsa(2,ntmax)); Allocate(snsasd(2,ntmax))
   snsa(:,:)=0.d0
   Allocate(ssntot(ntmax)); Allocate(ssntotsd(ntmax))
   Allocate(tyrm(ntmax))

open(unit=2, status='old' ,form='formatted', file=result_file)
do i=1, ntmax
   read(2, *) tyrm(i), snsa(1,i), snsa(2,i), ssntot(i) 
enddo
close(2)

i=1
do while (tyrm(i) .LE. styr) 
  i=i+1
enddo
itstart=i


findmax_flag=.FALSE.
!i=itstart
i=1
do while (tyrm(i) .LE. exyr) 
  i=i+1
enddo
exyr=i-1
do while (findmax_flag.EQ..FALSE.)
  if ((ssntot(i).LT.ssntot(i+1)).AND.(ssntot(i+1).GE.ssntot(i+2))) findmax_flag=.TRUE.
  i=i+1
enddo
solmax_sd=ssntot(i); tmax_sd=tyrm(i)

if (rank.EQ.0) then
  Allocate(solmax_arr(nprocs), tmax_arr(nprocs))
else
  Allocate(solmax_arr(0), tmax_arr(0))
endif
call MPI_Gather(solmax_sd, 1, MPI_real8, solmax_arr(1), 1, MPI_real8, 0, MPI_COMM_WORLD, ierr)
if (rank.EQ.0) then
! write(*,*) 'b4', solmax_arr(:)
 do i= nprocs-1, 2,-1
   do j=1, i 
    if (solmax_arr(j) .GE. solmax_arr(j+1)) then
       temp= solmax_arr(j)
       solmax_arr(j)=solmax_arr(j+1)
       solmax_arr(j+1)=temp
    endif    
   enddo
 enddo
! write(*,*) 'aft', solmax_arr(:)
 if (mod(nprocs,2).EQ.0) then
   med_solmax=0.5d0*(solmax_arr(nprocs/2)+solmax_arr(nprocs/2+1))
 else
   med_solmax=solmax_arr(nprocs/2+1)
 endif
endif
call MPI_Gather(tmax_sd, 1, MPI_real8, tmax_arr(1), 1, MPI_real8, 0, MPI_COMM_WORLD, ierr)
if (rank.EQ.0) then
! write(*,*) 'b4', tmax_arr(:)
 do i= nprocs-1, 2,-1
   do j=1, i 
    if (tmax_arr(j) .GE. tmax_arr(j+1)) then
       temp= tmax_arr(j)
       tmax_arr(j)=tmax_arr(j+1)
       tmax_arr(j+1)=temp
    endif    
   enddo
 enddo
! write(*,*) 'aft', tmax_arr(:)
 if (mod(nprocs,2).EQ.0) then
   med_tmax=0.5d0*(tmax_arr(nprocs/2)+tmax_arr(nprocs/2+1))
 else
   med_tmax=tmax_arr(nprocs/2+1)
 endif
 write(*,*) 'median solmax =', med_solmax
 write(*,*) 'median tmax =', med_tmax
endif
deallocate(solmax_arr, tmax_arr)

call MPI_Allreduce(solmax_sd, solmax, 1, MPI_real8, MPI_sum, Comm1)
solmax=solmax/real(nprocs, kind=8)
solmax_sd=(solmax_sd-solmax)*(solmax_sd-solmax)
call MPI_Allreduce(MPI_IN_PLACE, solmax_sd, 1, MPI_real8, MPI_sum, Comm1)
solmax_sd=dsqrt(solmax_sd/real(nprocs-1, kind=8))

call MPI_Allreduce(tmax_sd, tmax, 1, MPI_real8, MPI_sum, Comm1)
tmax=tmax/real(nprocs, kind=8)
tmax_sd=(tmax_sd-tmax)*(tmax_sd-tmax)
call MPI_Allreduce(MPI_IN_PLACE, tmax_sd, 1, MPI_real8, MPI_sum, Comm1)
tmax_sd=dsqrt(tmax_sd/real(nprocs-1, kind=8))


if (rank.EQ.0) then
  write(*,*) 'ssnmax, sd=', solmax, solmax_sd
  write(*,*) 'ssn t max, sd=', tmax, tmax_sd
endif


ncount=ntmax*2
snsasd(:,:)=snsa(:,:)
call MPI_Allreduce(snsasd, snsa, ncount, MPI_real8, MPI_sum, Comm1)
snsa(:,:)=snsa(:,:)/real(nprocs, kind=8)
snsasd=(snsasd-snsa)*(snsasd-snsa)
call MPI_Allreduce(MPI_IN_PLACE, snsasd, ncount, MPI_real8, MPI_sum, Comm1)
snsasd=dsqrt(snsasd/real(nprocs-1, kind=8))
ncount=ntmax
ssntotsd(:)=ssntot(:)
call MPI_Allreduce(ssntotsd, ssntot, ncount, MPI_real8, MPI_sum, Comm1)
ssntot(:)=ssntot(:)/real(nprocs, kind=8)
ssntotsd=(ssntotsd-ssntot)*(ssntotsd-ssntot)
call MPI_Allreduce(MPI_IN_PLACE, ssntotsd, ncount, MPI_real8, MPI_sum, Comm1)
ssntotsd=dsqrt(ssntotsd/real(nprocs-1, kind=8))




105 FORMAT(3E20.10)
106 FORMAT(E20.10,6E20.10)



if (rank.EQ.0) write(*,*) 'done!!!!'

!deallocate(Br, ssnNS, ssnNSer, tyro, pdego, tyrm)
deallocate(snsa, ssntot, snsasd, ssntotsd)
call MPI_FINALIZE(ierr)

end !subroutine !module data_obs
