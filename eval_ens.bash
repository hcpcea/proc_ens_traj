nens=10 #number of member in each small ensemble

listdir=outputlist.txt  #list of directories of every ensemble
TARGET=/beegfs/data/chung/ens/merged_n_B9_test_ex1986/  #output directory

#----end of iput here----


listsize=$(wc -l < $listdir)
tot=$((${nens}*${listsize}))
let nend=$nens+100
mkdir $TARGET


source load_env.bash
mpiifort -O3 compbrssnNS_model.f90 -o compbrssnNS_model
mpiifort -O3 comp_fmax_model_MPI_ASCII.f90 -o comp_fmax_model_MPI_ASCII
mpiifort -O3 comp_medmea_model_MPI_ASCII_v2.f90 -o comp_medmea_model_MPI_ASCII_v2

COUNTER2=101
while read src; do
#for src in ${list[@]}; do
  COUNTER=101
  while [ $COUNTER -le $nend ]; do
    echo $COUNTER
    ln -s $src/est_model_ssn_${COUNTER}.txt $TARGET/est_model_ssn_${COUNTER2}.txt
    ln -s $src/est_model_brad_${COUNTER}.txt $TARGET/est_model_brad_${COUNTER2}.txt
    let COUNTER=COUNTER+1
    let COUNTER2=COUNTER2+1
  done
done < $listdir


cp loop_ana_S.job loop_ana.job

sed -i "s|__nens__|$nens|g" loop_ana.job
sed -i "s|__listdir__|$listdir|g" loop_ana.job

cp mod_fmax_ASCII_S.txt mod_fmax_ASCII.txt
cp ens_fmax_S.job ens_fmax.job

sed -i "s|__merged_dir__|$TARGET|g" mod_fmax_ASCII.txt
sed -i "s|__tot__|$tot|g" ens_fmax.job


# first job - no dependencies
jid1=$(sbatch --parsable loop_ana.job)

# multiple jobs can depend on a single job
sbatch --dependency=afterok:$jid1 ens_fmax.job


#for plotting
cp ./plotpy/plot_SSN_csv_example.py .
sed -i "s|__OUTDIR__|$TARGET|g" plot_SSN_csv_example.py
cp ./plotpy/plot_contourB_csv.py .
sed -i "s|__OUTDIR__|$TARGET|g" plot_contourB_csv.py


