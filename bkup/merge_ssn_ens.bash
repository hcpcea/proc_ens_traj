TARGET=/beegfs/data/chung/ens/merged_n_B9_icAuto_ex2022pt2_pos_padconst_add_ampICpt00003MCpt2/
#list=(
#/beegfs/data/chung/ens/n_B9_icAuto_ex2021pt9_pos_fssn00050_add_ampICpt00003MCpt2
#)

list=outputlist.txt

source load_env.bash
mpiifort -O3 comp_fmax_model_MPI_ASCII.f90 -o comp_fmax_model_MPI_ASCII
mpiifort -O3 comp_medmea_model_MPI_ASCII_v2.f90 -o comp_medmea_model_MPI_ASCII_v2

nens=64
listsize=$(wc -l < $list)
tot=$((${nens}*${listsize}))
let nend=$nens+100
mkdir $TARGET


COUNTER2=101
while read src; do
#for src in ${list[@]}; do
  COUNTER=101
  while [ $COUNTER -le $nend ]; do
    echo $COUNTER
    ln -s $src/est_model_ssn_${COUNTER}.txt $TARGET/est_model_ssn_${COUNTER2}.txt
    let COUNTER=COUNTER+1
    let COUNTER2=COUNTER2+1
  done
done < $list


cp mod_fmax_ASCII_S.txt mod_fmax_ASCII.txt
cp ens_fmax_S.job ens_fmax.job

sed -i "s|__merged_dir__|$TARGET|g" mod_fmax_ASCII.txt
sed -i "s|__tot__|$tot|g" ens_fmax.job
