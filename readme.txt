eval_ens.bash is the main bash script to compile and launch all the 
exe.
And mod_fmax_ASCII_S.txt is the namelist to fill.
outputlist.txt list of DA runs directories.
load_env.bash  is the bash script specify module needed.

1) fill the namelist mod_fmax_ASCII_S.txt and outputlist.txt
the settingin eval_ens.bash, 2) launch by 
bash eval_ens.bash

source codes 
compbrssnNS_model.f90                calculate SSN and surface Br from the trajectory.
comp_fmax_model_MPI_ASCII.f90        locate the forecast SSN max
comp_medmea_model_MPI_ASCII_v2.f90   output the csv file of forecast  

job scripts
loop_ana.job                         launch compbrssnNS_model.f90
ens_fmax.job                         launch comp_fmax_model_MPI_ASCII.f90 
                                     and comp_medmea_model_MPI_ASCII_v2.f90
